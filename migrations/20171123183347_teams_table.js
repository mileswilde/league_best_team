exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('teams', function(table){
      table.increments('id').primary().unsigned();
      table.bigInteger('game_id');
      table.boolean('win');
      table.integer('player1');
      table.string('player1_lane');
      table.integer('player2');
      table.string('player2_lane');
      table.integer('player3');
      table.string('player3_lane');
      table.integer('player4');
      table.string('player4_lane');
      table.integer('player5');
      table.string('player5_lane');
      table.integer('side');
    }),
    knex.schema.createTable('character_combinations', function(table){
      table.increments('id').primary().unsigned();
      table.integer('character1');
      table.integer('character2');
      table.float('games_played');
      table.float('games_won');
      table.float('winrate')
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('teams'),
    knex.schema.dropTable('character_combinations')
  ])
};