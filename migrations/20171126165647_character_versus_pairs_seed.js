exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('character_versus', function(table){
      table.increments('id').primary().unsigned();
      table.integer('character1');
      table.integer('character2');
      table.float('games_played');
      table.float('games_won');
      table.float('winrate');
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('character_versus')
  ])
};