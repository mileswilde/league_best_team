var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var champion_ids = require("./id_to_champ");

function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}


// for (let i = 0; i < 10; i++){
//   // prints out all champion ids of both teams

//   console.log(id_to_champ.id_to_champ(games_obj.matches["0"].participants[i].championId))
// }
var games_played = 0.0;
var games_won = 0.0;
for (let k = 1; k < 11; k++){
  var games_obj = JSON.parse(Get("https://s3-us-west-1.amazonaws.com/riot-developer-portal/seed-data/matches"+k+".json"));
  for (let j = 0; j < 100; j++){
    for (let i = 0; i < 5; i++){
      // prints out all champion of both teams
      var current_champ = champion_ids.id_to_champ(games_obj.matches[j].participants[i].championId)

      if(current_champ == "Nasus"){
        games_played++;
        if(games_obj.matches[j].teams[0].win == "Win"){
          games_won++;
        }
      }
    }  
    for (i = 5; i < 10; i++){
      // prints out all champion ids of both teams
      var current_champ = champion_ids.id_to_champ(games_obj.matches[j].participants[i].championId)
      if(current_champ == "Nasus"){
        games_played++;
        if(games_obj.matches[j].teams[1].win == "Win"){
          games_won++;
        }
      }
    }
  }
}

console.log(games_won/games_played)
console.log(games_won)
console.log(games_played)