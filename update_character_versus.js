require('dotenv').config({path: __dirname + '/.env'})

var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: "league_development",
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});
// this is ugly
var all_teams = []
var flag = 0;
knex('teams')
  .where('id', '>', 0)
  .then(async all_teams =>{
    for (let l = 0; l < all_teams.length; l = l + 2){
      // team 1 position
      for (var i = 1; i < 6; i++){
        // team 2 position
        for (var k = 1; k < 6; k++){
          // only works if this where clause always returns a sorted array of objects ascending
          var team_1 = all_teams[l]
          var team_2 = all_teams[l+1]
          await update_pair(team_1["player" + i], team_2["player" + k], team_1.win)
          await update_pair(team_2["player" + k], team_1["player" + i], team_2.win)
          flag++;
          console.log("flag: ",flag)
        }
      }
    }
    console.log("Updating winrate")
    await update_winrate()
  })
  .catch(error =>{
    console.log(error)
  })
  
// need to start with a id, side = 1, select first champ in that team, 


async function update_pair(id1, id2, win){
  if (win == 1){
    return knex('character_versus')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: knex.raw('games_won + 1'),
        winrate: undefined
      })
      .catch(error =>{
        console.log(error)
      })
      .then()
  }
  else {
    return knex('character_versus')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: undefined,
        winrate: undefined

      })
      .catch(error =>{
        console.log(error)
      })
      .then()
  }
}

async function update_winrate(){
    return knex('character_versus')
      .whereNot('games_played',0)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: undefined,
        games_won: undefined,
        winrate: knex.raw('games_won/games_played')
      })
      .catch(error =>{
        console.log(error)
      })
      .then()
}