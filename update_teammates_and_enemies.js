require('dotenv').config({path: __dirname + '/.env'})

var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: "league_development",
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});
var all_teams = []
var flag = 0;
knex('teams')
// this is ugly
  .where('id', '>', 0)
  .then(res =>{
    for (var team of res){
      for (var i = 1; i < 5; i++){
        for (var k = i + 1; k < 6; k++){
          // console.log("i", team["player" + i])
          // console.log("k", team["player" + k])
          update_pair(team["player" + i], team["player" + k], team.win)
          update_pair(team["player" + k], team["player" + i], team.win)          
          flag++;
          console.log("flag: ",flag)
        }
      }
    }
    console.log("Updating teammate winrate")
    update_winrate()
  })



function update_pair(id1, id2, win){
  if (win == 1){
    knex('character_combinations')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: knex.raw('games_won + 1'),
        winrate: undefined
      })
      .then()
  }
  else {
    knex('character_combinations')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: undefined,
        winrate: undefined

      })
      .then()
  }
}

function update_winrate(){
    knex('character_combinations')
      .whereNot('games_played',0)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: undefined,
        games_won: undefined,
        winrate: knex.raw('games_won/games_played')
      })
      .then()
}

// this is ugly
var all_teams = []
var flag = 0;
knex('teams')
  .where('id', '>', 0)
  .then(all_teams =>{
    for (let l = 0; l < all_teams.length; l = l + 2){
      // team 1 position
      for (var i = 1; i < 6; i++){
        // team 2 position
        for (var k = 1; k < 6; k++){
          var team_1 = all_teams[l]
          var team_2 = all_teams[l+1]
          update_pair_versus(team_1["player" + i], team_2["player" + k], team_1.win)
          update_pair_versus(team_2["player" + k], team_1["player" + i], team_2.win)
          flag++;
          console.log("flag: ",flag)
        }
      }
    }
    console.log("Updating versus winrate")
    update_winrate_versus()
  })
// need to start with a id, side = 1, select first champ in that team, 


function update_pair_versus(id1, id2, win){
  if (win == 1){
    knex('character_versus')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: knex.raw('games_won + 1'),
        winrate: undefined
      })
      .then()
  }
  else {
    knex('character_versus')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: undefined,
        winrate: undefined

      })
      .then()
  }
}

function update_winrate_versus(){
    knex('character_versus')
      .whereNot('games_played',0)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: undefined,
        games_won: undefined,
        winrate: knex.raw('games_won/games_played')
      })
      .then()
}