require('dotenv').config({path: __dirname + '/.env'})

var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: "league_development",
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});
var all_teams = []
var flag = 0;
knex('teams')
// this is ugly
  .where('id', '>', 0)
  .then(async res =>{
    console.log(res.length)
    for (var team of res){
      for (var i = 1; i < 5; i++){
        for (var k = i + 1; k < 6; k++){
          await update_pair(team["player" + i], team["player" + k], team.win)
          await update_pair(team["player" + k], team["player" + i], team.win)          
          flag++;
          console.log("flag: ",flag)
        }
      }
    }
    console.log("Updating winrate")
    await update_winrate().then("updated")
  })
  .catch(error =>{
    console.log(error)
  })
  
async function update_pair(id1, id2, win){
  if (win == 1){
    return knex('character_combinations')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: knex.raw('games_won + 1'),
        winrate: undefined
      })
      .catch(error =>{
        console.log(error)
      })
      .then()
  }
  else {
    return knex('character_combinations')
      .where('character1', id1)
      .andWhere('character2', id2)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: knex.raw('games_played + 1'),
        games_won: undefined,
        winrate: undefined

      })
      .catch(error =>{
        console.log(error)
      })
      .then()
  }
}

function update_winrate(){
    return knex('character_combinations')
      .whereNot('games_played',0)
      .update({
        character1: undefined,
        character2: undefined,
        games_played: undefined,
        games_won: undefined,
        winrate: knex.raw('games_won/games_played')
      })
      .catch(error =>{
        console.log(error)
      })
      .then()
}