var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var champion_ids = require("./id_to_champ");
require('dotenv').config({path: __dirname + '/.env'})

function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: "league_development",
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});

total_time = 0;
// knex('teams')
//   .distinct('player1')
//   .then(res=>{
//     for (player of res){
//       console.log(player.player1) 
//     }
//     console.log(res)
//   })

knex('teams')
  .max('game_id')
  .then(max_game_id => {
    if (max_game_id[0].max < 1000){ 
      var max_game = 2658920000
      adding_games(max_game)
      console.log("for a total of ", total_time/1000,"s")
    }
    else {
      var max_game = max_game_id[0].max
      // max_game = 2658820000 // for if matchId gets into tutorial/custom game zone
      adding_games(Number(max_game) + 1)
      console.log("for a total of ", total_time/1000,"s")
    }    
  })

async function adding_games(current_match){
  var undefined_counter = 0;
  for (let k = 1; k < 200000; k++){
    var start = new Date();
    var startTime = start.getTime();

    current_match++

    // var games_obj = JSON.parse(Get(`https://na1.api.riotgames.com/lol/match/v3/matches/${current_match}?api_key=${process.env.API_KEY}`));
    var games_obj = JSON.parse(Get(`https://na1.api.riotgames.com/lol/match/v3/matches/${current_match}?api_key=${process.env.API_KEY}`));
    console.log(games_obj.gameType, games_obj.gameMode)
    if (games_obj.gameType === 'TUTORIAL_GAME' || games_obj.gameType === 'CUSTOM_GAME'){
      current_match+=1000
    }
    // if (games_obj.gameType !== undefined && games_obj.gameType !== "TUTORIAL_GAME" || games_obj.gameType !== "CUSTOM_GAME"){
    //   console.log("bot match?")
    //   if (games_obj.participantIdentities[5].accountId === 0){
    //     console.log("BOT MATCH")
    //   }
    // }
    if (games_obj.gameType === undefined){
      undefined_counter++
      console.log("undefined counter", undefined_counter)
      if (undefined_counter > 20){
        current_match+=1000
        undefined_counter=0
      }
    }
    if (games_obj.gameType !== undefined && games_obj.gameType === 'MATCHED_GAME'){
      if (games_obj.mapId === 11 && games_obj.participantIdentities[5].accountId !== 0){
        undefined_counter = 0
        var team1_array = [];
        var team2_array = [];
        var team1_win = false;
        var team2_win = false;
        var game_id = games_obj.gameId

        // ***** TEAM 1 *******

        // gets all champ ids for team 1
        for (let i = 0; i < 5; i++){
          // adding champ ids to team1 array
          team1_array.push(
            {
              id: games_obj.participants[i].championId, 
              lane: games_obj.participants[i].timeline.lane
            })
        }
        if(games_obj.teams[0].win === "Win"){
          team1_win = true
        }
        var team1_obj = {
          game_id: game_id, 
          win: team1_win, 
          player1: team1_array[0].id, 
          player1_lane: team1_array[0].lane, 
          player2: team1_array[1].id, 
          player2_lane: team1_array[1].lane, 
          player3: team1_array[2].id, 
          player3_lane: team1_array[2].lane, 
          player4: team1_array[3].id, 
          player4_lane: team1_array[3].lane, 
          player5: team1_array[4].id, 
          player5_lane: team1_array[4].lane, 
          side: 1
        }
        // inserting team1 for current game

        // ***** TEAM 2 *******

        // gets all champ ids for team 2
        for (i = 5; i < 10; i++){
          // adding champ ids to team2 array
          team2_array.push(
            {
              id: games_obj.participants[i].championId, 
              lane: games_obj.participants[i].timeline.lane
            })        }
        if(games_obj.teams[1].win === "Win"){
          team2_win = true
        }
        // inserting team2 for current game
        var team2_obj = {
          game_id: game_id, 
          win: team2_win, 
          player1: team2_array[0].id, 
          player1_lane: team2_array[0].lane, 
          player2: team2_array[1].id, 
          player2_lane: team2_array[1].lane, 
          player3: team2_array[2].id, 
          player3_lane: team2_array[2].lane, 
          player4: team2_array[3].id, 
          player4_lane: team2_array[3].lane, 
          player5: team2_array[4].id, 
          player5_lane: team2_array[4].lane, 
          side: 2
        }
        insert_object(team1_obj)
        insert_object(team2_obj)
      }
    }

    await sleep(540);
    var end = new Date();
    var endTime= end.getTime();
    var timeTaken = end - start;
    console.log(timeTaken/1000, "s")
    console.log(current_match)
  };
} 

async function insert_object(object){
  await knex('teams').insert(object).then(console.log("done"))
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}