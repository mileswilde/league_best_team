# This project was created because I had an interest in figuring out which champions had the best winrates,
# which champions were best against others,  the best team compositions in the video game 'League of Legends'
# This was created using NodeJS, PostgreSQL, and EJS templates.
# The main files are 'looking_at_match_api.js' which goes to the League of Legends API and pulls and formats games into our database.
# Then the files 'update_character_combinations.js', 'update_character_versus.js', 'update_teammates_and_enemies.js' all update the database with statistics
# surrounding the characters and their winrates with and against other champions.
# The 'server.js' really only renders the main page and has a button where you can search specific champions, then it gets all the info and displays it.

# to use this, just clone it, type npm install and npm start, then connect to localhost:8080.
