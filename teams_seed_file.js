require('dotenv').config({path: __dirname + '/.env'})
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
require('dotenv').config()
var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: "league_development",
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});
function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

var all_teams_array = []
for (let k = 1; k < 11; k++){
  var games_obj = JSON.parse(Get("https://s3-us-west-1.amazonaws.com/riot-developer-portal/seed-data/matches"+k+".json"));
  console.log(games_obj)
  for (let j = 0; j < 100; j++){
    var team1_array = [];
    var team2_array = [];
    var team1_win = false;
    var team2_win = false;
    var game_id = games_obj.matches[j].gameId

    // ***** TEAM 1 *******

    // gets all champ ids for team 1
    for (let i = 0; i < 5; i++){
      // adding champ ids to team1 array
      team1_array.push(games_obj.matches[j].participants[i].championId)
    }
    if(games_obj.matches[j].teams[0].win == "Win"){
      team1_win = true
    }
    var team1_obj = {game_id: game_id, win: team1_win, player1: team1_array[0], player2: team1_array[1], player3: team1_array[2], player4: team1_array[3], player5: team1_array[4], side: 1}
    // inserting team1 for current game

    // ***** TEAM 2 *******

    // gets all champ ids for team 2
    for (i = 5; i < 10; i++){
      // adding champ ids to team2 array
      team2_array.push(games_obj.matches[j].participants[i].championId)
    }
    if(games_obj.matches[j].teams[1].win == "Win"){
      team2_win = true
    }
    // inserting team2 for current game
    team2_obj = {game_id: game_id, win: team2_win, player1: team2_array[0], player2: team2_array[1], player3: team2_array[2], player4: team2_array[3], player5: team2_array[4], side: 2}          
    all_teams_array.push(team1_obj)
    all_teams_array.push(team2_obj)
    knex('teams').insert(team1_obj)
  }
}
insert_everything(all_teams_array)


function insert_everything(array){
  for (var obj of array){
    knex('teams').insert(obj).then(console.log(array.indexOf(obj)))
  }
}