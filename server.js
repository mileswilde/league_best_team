var express = require("express");
var lies = require("./main_alg_lies");
var app = express();
var PORT = process.env.PORT || 8080; // default port 8080
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: true}));

var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: "league_development",
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});

app.use(express.static('public'));
app.use('/scripts', express.static('public'));
app.use('/styles', express.static('public'));

app.set("view engine", "ejs")

app.get("/", (req, res) => {
  res.render("home_page",{name_out:[],win_out:[]});
});

app.post("/champ_ids", (req, res) =>{

	var find_champ = lies.main_alg_lies(req.body.select,req.body.select2)
		.then(find_champ=>{
			console.log(find_champ)
				var champ_vars = {
					name_out:find_champ.name,
					win_out:find_champ.winrate
				}
			res.render("home_page",champ_vars)
		})

	//res.render("home_page",champ_vars)
});

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}!`);
});