require('dotenv').config({path: __dirname + '/.env'})
var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: 'league_development',
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});

var id_name = require("./id_to_champ");

var champion_ids = [266,412,23,79,69,136,13,78,14,1,202,43,111,240,99,103,2,112,34,27,86,
127,57,25,28,105,74,238,68,82,37,96,55,117,22,30,12,122,67,110,77,89,
126,134,80,92,121,42,268,51,76,85,3,45,432,150,90,104,254,10,39,64,420,
60,106,20,4,24,102,429,36,427,131,223,63,113,8,154,421,133,84,163,18,
120,15,236,107,19,72,54,157,101,17,75,58,119,35,50,91,40,115,245,61,114,
9,31,33,7,16,26,56,222,83,6,203,21,62,53,98,201,5,29,11,44,32,41,48,38,
161,143,267,59,81]

//var team_champs = [86,19,134,40];
//var enemy_champs = [17,64,103,236,412];
var champ_rating = [];

//champ_choice(team_champs,enemy_champs);

async function champ_choice(team_champs,enemy_champs){
	var champ_out = [];
	var array = [];
	var array2 = [];
	if(!Array.isArray(team_champs)){
		array.push(team_champs);
		team_champs = array;
	}
	if(!Array.isArray(enemy_champs)){
		array2.push(enemy_champs);
		enemy_champs = array2;
	}
	console.log(team_champs);		
	console.log(enemy_champs);			
	console.log(team_champs.length);		
	console.log(enemy_champs.length);
	
	for(var i = 0; i < champion_ids.length; i++){
		champ_rating[i] = await weight_alg(champion_ids[i],team_champs,enemy_champs);
		champ_out[i] = [];
	}
	for(var i = 0; i < champion_ids.length; i++){
		//console.log(champion_ids[i]," total rating: ",champ_rating[i])
		champ_out[i][1] = id_name.id_to_champ(champion_ids[i]);
		champ_out[i][0] = Number(champ_rating[i]).toFixed(2);
		//console.log(champ_out[i][1]," total rating: ",champ_out[i][0])
	}
	
	champ_out.sort(function(a,b){
			return b[0] - a[0]
	});
	for(var i =0; i<team_champs.length;i++){
		console.log("For Teammates: ", id_name.id_to_champ(team_champs[i]));
	}
	for(var i =0; i<enemy_champs.length;i++){
		console.log("For Enemies: ", id_name.id_to_champ(enemy_champs[i]));
	}
	
		
	for(var i = 0; i < champion_ids.length; i++){
		console.log(champ_out[i][1]," Win percentage: ",champ_out[i][0])
	}
	var top_five = {
		name:[],
		winrate:[]
	};
	for(var i =0;i<5;i++){
		top_five.name[i] = champ_out[i][1];
		top_five.winrate[i] = champ_out[i][0];
;	}
	return(top_five)
	//sorted_champs = sort(champ_rating);
	//return(sorted_champs);
}

async function weight_alg(your_champ, team_champs,enemy_champs){
	var weight = await team_winrate(your_champ, team_champs);
	//console.log('we',weight);
	weight += await enemy_winrate(your_champ, enemy_champs);
	//weight += (weight)* your_ability(your_champ);
	return(weight);
}

async function team_winrate(your_champ, team_champs){
	var total_winrate =0;
	for(i=0;i< team_champs.length;i++){
		//console.log('your',your_champ, 'team: ',team_champs[i]);
		total_winrate += await ally_winrate(your_champ,team_champs[i]);
		total_winrate += await ally_winrate(team_champs[i],your_champ);
	}
	//total_winrate=total_winrate/team_champs.length;
	return(total_winrate);
}	


async function enemy_winrate(your_champ, enemy_champs){
	var total_winrate = 0;
	for(i=0;i< enemy_champs.length;i++){
		//console.log('your',your_champ, 'team: ',team_champs[i]);
		total_winrate += await ally_winrate(your_champ,enemy_champs[i]);
		total_winrate += await ally_winrate(enemy_champs[i],your_champ);
	}
	//total_winrate=total_winrate/team_champs.length;
	return(total_winrate);
}	


function ally_winrate(id1,id2){
	// var winrate;
  return (
    knex('character_combinations')
      .where('character1', id1)
      .andWhere('character2', id2)
      .then(res=>{//console.log('res',res);
        if(res.length > 0 ){
          //console.log(res[0].winrate);
          var winrate = res[0].winrate;
          //console.log('winrate: ',winrate);
          //console.log(res[0])
          //console.log("should be returning and adding this: ", winrate)
          return(winrate);
        }else{
          return(0);
        }
      }) 
  //console.log(winrate); 
  //return(winrate);
  )
}


function opp_winrate(id1,id2){
	// var winrate;
  return (
    knex('character_versus')
      .where('character1', id1)
      .andWhere('character2', id2)
      .then(res=>{//console.log('res',res);
        if(res.length > 0 ){
          //console.log(res[0].winrate);
          var winrate = res[0].winrate;
          //console.log('winrate: ',winrate);
          //console.log(res[0])
          //console.log("should be returning and adding this: ", winrate)
          return(winrate);
        }else{
          return(0);
        }
      })  
  //console.log(winrate); 
  //return(winrate);
  )
}


module.exports = {
    main_alg_lies: champ_choice
}



