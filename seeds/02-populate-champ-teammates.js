
var champ_ids = [106,120,8,497,80,16,54,412,103,115,236,110,99,48,83,28,
36,15,77,4,30,119,40,122,56,53,62,268,222,92,23,127,101,20,44,82,25,58,1,
26,117,111,136,86,13,22,91,45,238,27,60,105,21,75,5,245,114,43,498,11,126,39,429,432,112,254,3,131,201,61,96,67,14,107,17,
142,89,121,50,33,161,157,19,154,57,51,35,31,76,69,203,37,85,34,81,421,32,12,10,79,164,42,90,18,134,59,78,143,
98,133,150,420,427,113,63,9,24,64,55,68,84,38,240,223,74,104,163,102,6,29,2,267,72,41,516,202,141,266,7]

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('character_combinations').del()
    .then(function () {
      // Inserts seed entries
      array_to_insert = create_insert_array(champ_ids)
      return Promise.all(array_to_insert)
    });
};


var knex = require('knex')({
  client: 'postgresql',
  connection: {
    database: "league_development",
    user: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});
async function create_insert_array(champion_ids){
  insert_array = []
  count = 0
  for (let champ in champion_ids){
    for (let pair = (Number(champ) + 1); pair < champion_ids.length; pair++){
      count++
      insert_array[count] = await knex('character_combinations').insert({
        character1: champion_ids[champ],
        character2: champion_ids[pair],
        games_played: 0,
        games_won: 0
      })
    }
  }
  return insert_array
}